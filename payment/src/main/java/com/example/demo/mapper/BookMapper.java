package com.example.demo.mapper;



import com.example.demo.dto.BookDto;
import com.example.demo.entity.Book;
import org.mapstruct.Mapper;

import java.util.List;

@Mapper(componentModel = "spring")
public interface BookMapper {

    BookDto bookToBookDto(Book book);

    List<BookDto> bookToBookDtoList(List<Book> bookList);


}
